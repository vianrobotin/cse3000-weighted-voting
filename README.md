# Running the experiments

To run the experiments for Narwhal, simply run the `optimWeightsForLatency.py` file. Changing the parameters can be done in line 390 and 381. 

To run the experiments for Narwhal and Tusk, navigate to `narwhal-master/benchmark` and run `fab local`. The quorum threshold can be set at line 172 in `narwhal-master/config/src/lib.rs`. We introduced the latency matrix in `narwhal-master/primary/src/core.rs`. This is where we put nodes to sleep to simulate latency. Changing the weight assignment can be done at line 85 in `narwhal-master/benchmark/benchmark/config.py`, 