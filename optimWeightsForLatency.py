from collections import Counter
import numpy as np
import random
import heapq
import matplotlib.pyplot as plt
from itertools import chain, combinations, count, islice, repeat
import string
import scipy.special
import time
import matrix_utils as m_utils
##import triplet_utils as t_utils
import math
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

plt.rcParams.update({'font.size': 16})

# Define the data
data = {
    "Source Region": ["Africa (Cape Town)", "Asia Pacific (Hong Kong)", "Asia Pacific (Tokyo)", "Asia Pacific (Seoul)", 
                      "Asia Pacific (Osaka)", "Asia Pacific (Mumbai)", "Asia Pacific (Singapore)", "Asia Pacific (Sydney)", 
                      "Canada (Central)", "EU (Frankfurt)", "EU (Stockholm)", "EU (Milan)", "EU (Ireland)", 
                      "EU (London)", "EU (Paris)", "Middle East (Bahrain)", "SA (Sao Paulo)", "US East (N. Virginia)", 
                      "US East (Ohio)", "US West (N. California)", "US West (Oregon)"],
    "Africa (Cape Town)": [11.16,	352.48,	353.73,	383.33,	360.79,	264.67,	318.21,	409.78,	222.3,	154.2,	177.34,	145.96,	157.23,	149.05,	143.89,	206.84,	336.05,	226.79,	237.5,	290.61,	273.51],
    "Asia Pacific (Hong Kong)": [357.08,	1.2,	55.6,	38.15,	49.75,	94.61,	38.43,	130.16,	196.39,	219.89,	239.72,	197.17,	251.66,	211.76,	203.35,	126.31,	308.66,	200.79,	185.93,	155.81,	146.63],
    "Asia Pacific (Tokyo)": [357.13,	53.92,	4.06	,35.68	,10.22	,136.85	,72.41	,110.34	,143.77	,253.05	,273.53	,239.95	,202.51	,211.51	,216.74	,166.16	,256.84	,155.1	,134.39	,109.14	,97.92],
    "Asia Pacific (Seoul)": [391.49,	39.44	,35.42	,4.55	,24.42	,131.06	,73.19	,145.85	,175.02	,257.46	,277.16	,234.75	,231.5	,241.9	,248.99	,160.7	,285.35	,181.46	,164.86	,131.11	,122.51],
    "Asia Pacific (Osaka)": [364.54,	49.0	,10.82	,23.44	,1.37	,133.5	,78.87	,119.73,	150.04,	257.26,	276.78,	233.87,	205.99,	216.37,	222.79,	158.53,	261.64,	153.9,	140.02,	111.69,	100.28],
    "Asia Pacific (Mumbai)": [267.47,	96.18,	138.04,	131.95,	134.22,	2.86	,60.83	,153.09	,192.23	,130.94	,149.75	,109.4	,133.86	,118.98	,110.26,	36.26	,304.32,	196.96,	204.86,	223.97,	213.77],
    "Asia Pacific (Singapore)": [323.68,	38.44	,71.44	,74.46	,79.37,	57.18,	3.12,	94.16	,212.48	,186.49	,204.48	,160.75	,233.36	,172.74	,162.77,	92.5	,327.78,	232.7	,202.12,	170.17,	160.5],
    "Asia Pacific (Sydney)": [414.84	,130.14	,110.0,	145.34,	119.38	,151.57	,96.12	,2.1	,201.62,	278.87,	296.16,	258.05	,255.84	,267.62	,281.65	,184.67	,312.67	,202.5	,188.96,	139.51,	142.35,],
    "Canada (Central)": [224.61,	196.31,	145.12,	179.45,	150.76,	193.95,	211.66,	199.27,	2.69,	93.16	,110.57	,102.4	,71.16,	81.31,	85.08	,161.91,	125.54,	17.62,	26.81,	80.48,	62.85],
    "EU (Frankfurt)": [158.07,	218.52,	254.93,	256.65,	257.38,	130.86,	187.49,	279.3,	93.24	,4.45,	22.17,	12.62,	27.43,	19.36	,12.02,	87.04,	203.93,	92.63	,103.93,	155.24	,143.62],
    "EU (Stockholm)": [181.73,	239.4	,273.11	,277.55	,279.29,	149.01,	204.68,	295.35,	108.76,	24.39,	3.1	,32.31,	43.61	,34.25,	32.1,	105.4	,216.56,	111.27,	125.22,	173.44,	158.49],
    "EU (Milan)": [149.19,	196.53	,240.41	,234.68	,236.18,	110.46	,163.06	,256.93	,103.31	,12.38	,31.5	,1.69	,38.29	,28.65	,21.43,	109.94,	212.58,	103.64,	114.47,	162.91,	152.72],
    "EU (Ireland)": [162.05,	253.16	,200.88	,230.87	,208.57	,132.25,	234.98,	256.02	,69.13	,27.93	,43.08	,35.69	,4.19	,13.88	,19.75	,104.65,	178.2,	70.32	,81.02	,133.31,	119.22],
    "EU (London)": [150.0,	210.34,	210.11,	241.55,	216.82,	120.33,	171.67,	267.89,	80.18,	18.76,	33.41,	26.2,	13.63,	3.41,	12.39,	89.41,	186.85,	78.34,	90.07,	148.34,	129.37],
    "EU (Paris)": [153.39,	203.38	,217.49	,246.89	,223.22	,112.14	,162.49	,281.1	,85.84	,12.32	,31.63	,21.03	,19.33	,14.61	,2.05	,86.19	,196.98	,84.18,	95.68	,146.82	,134.8],
    "Middle East (Bahrain)": [212.17,	125.72,	166.28,	161.33,	157.95,	37.56,	89.66,	183.99,	161.48,	88.27,	104.31,	109.86,	103.57,	91.39,	84.19,	2.45,	273.46,	168.98,	173.7,	223.42,	212.25],
    "SA (Sao Paulo)": [341.55,	307.56,	256.32	,286.37	,261.65	,304.71	,326.65,	311.18,	126.18,	204.53,	216.54,	213.73,	178.19,	188.81,	194.68,	274.08,	2.06,	114.96,	125.77,	176.3,	174.17],
    "US East (N. Virginia)": [227.4,	201.11,	158.97,	177.63,	152.29,	196.55,	232.2,	200.69,	18.07	,95.65	,112.78	,103.08	,69.51	,76.9	,85.64	,165.5,	118.1	,6.52	,15.53	,66.3	,64.65],
    "US East (Ohio)": [242.58	,184.77,	135.19,	162.77,	138.74,	206.59,	202.37,	189.46,	30.8	,107.58	,128.01,	112.86,	84.44	,89.38	,96.86	,173.33,	125.55,	19.9	,6.61	,57.11	,58.46],
    "US West (N. California)": [291.54,	159.42,	107.85,	131.43,	109.09,	225.85,	169.9,	139.2	,79.83	,154.39,	172.87,	163.43,	130.61,	147.65,	145.94,	221.02,	176.54,	64.8	,52.66	,3.2,	21.88],
    "US West (Oregon)": [275.36,	146.6	,97.62	,120.88	,98.81	,215.16,	159.4,	141.66,	60.4	,141.87,	158.05,	151.2	,117.95	,130.01,	135.23,	213.0,	174.36,	64.13	,50.03	,23.18,	1.95]
}

# Create a DataFrame
df = pd.DataFrame(data)

# print(df)

# Set 'Source Region' as the index
df.set_index("Source Region", inplace=True)

# # Create a heatmap
# plt.figure(figsize=(16, 12))
# sns.heatmap(df, fmt=".2f", cmap="coolwarm", linewidths=.5)
# plt.xticks(fontsize=16)
# plt.yticks(fontsize=16)
# plt.show()

from collections import defaultdict



class Block:
  def __init__(self, creator, round, transactions, certificates):
    self.creator = creator
    self.round = round
    self.transactions = transactions
    self.certificates = certificates
    self.digest = hash((self.creator, self.round, tuple(transactions), tuple(certificates)))

class Certificate:
  def __init__(self, block_digest, round, creator):
    self.block_digest = block_digest
    self.round = round
    self.creator = creator

class Validator:
  def __init__(self, id, weight, quorum_weight, faulty_replica=False):
    self.faulty_replica = faulty_replica
    self.quorum_weight = quorum_weight
    self.id = id
    self.all_validators=[]
    self.round = 0
    self.transactions = []
    self.certificates = defaultdict(list)  # round -> list of certificates
    self.certified_blocks = []
    self.own_block = None
    self.acks=0
    self.weight = weight
    self.acks_time = []
    self.all_acks_time = 0
    self.certificates_time = []
    self.all_cerificates_time = 0
    self.certificate_creation_times = []
    self.block_creation_times = []

  def receive_ack(self, weight, latency):
    heapq.heappush(self.acks_time, (latency, weight))
    if(len(self.acks_time) == len(self.all_validators)):
        weight = 0
        while weight < self.quorum_weight:
            (rcvTime, rcvWeight) = heapq.heappop(self.acks_time)
            weight += rcvWeight
            self.all_acks_time = rcvTime
        self.create_certificate()

  def receive_transaction(self, tx):
    self.transactions.append(tx)
    if(len(self.transactions) >= tx_per_block):
        self.create_block()

  def create_block(self):
    if self.own_block:
      return
    if(self.round == 0):
        self.own_block = Block(self.id, self.round, [], [])
        self.broadcast_block()
    elif(sum(list(map(lambda x: self.all_validators[x.creator].weight , self.certificates[self.round-1])))>=self.quorum_weight): #WHEAT weight requirement 
            self.own_block = Block(self.id, self.round, self.transactions, self.certificates[self.round-1])
            self.broadcast_block()

  def broadcast_block(self):
    for validator in self.all_validators:
      validator.receive_block(self.own_block)

  def receive_block(self, block: Block):
    if block.round == 0 or (block.round == self.round and sum(list(map(lambda x:  self.all_validators[x.creator].weight , block.certificates)))>=self.quorum_weight):
      if(self.faulty_replica):
        self.all_validators[block.creator].receive_ack(self.weight, 1000000000)
        return
      self.certified_blocks.append(block)
      self.all_validators[block.creator].receive_ack(self.weight, self.all_cerificates_time + latency_matrix[self.id][block.creator] + latency_matrix[block.creator][self.id])
    elif block.round > self.round:
      self.certificates[block.round].append(block)

  def advance_round(self):
    self.certificate_creation_times.append(self.all_acks_time)
    self.block_creation_times.append(self.all_cerificates_time)
    self.round += 1
    self.acks = 0
    self.own_block = None
    self.acks_time = []
    self.certificates_time = []
    if self.certificates[round]:
       for block in self.certificates[round]:
          self.receive_block(block)

  def create_certificate(self):
    if not self.own_block:
      return
    certificate = Certificate(self.own_block.digest, self.own_block.round, self.own_block.creator)
    self.broadcast_certificate(certificate)

  def broadcast_certificate(self, certificate: Certificate):
    if(not self.faulty_replica):
      for validator in self.all_validators:
        validator.receive_certificate(certificate, self.all_acks_time + latency_matrix[self.id][validator.id])
    else:
      for validator in self.all_validators:
        validator.receive_certificate(certificate, 1000000000)

  def receive_certificate(self, certificate: Certificate, latency):
    self.certificates[certificate.round].append(certificate)
    heapq.heappush(self.certificates_time, (latency, self.all_validators[certificate.creator].weight))
    if(len(self.certificates_time) == len(self.all_validators)):
        weight = 0
        while weight < self.quorum_weight:
            (rcvTime, rcvWeight) = heapq.heappop(self.certificates_time)
            weight += rcvWeight
            self.all_cerificates_time = rcvTime

def formQV(n, latency_matrix, weights, quorum_weight): # Alg. 1 in AWARE

    received = []
    for i in range(n):
        received.append([])
        for j in range(n):
              heapq.heappush(received[i], (latency_matrix[j][i] + latency_matrix[i][j], weights[j])) # use received[] as a priority queue

    completion_time = [0] * n
    
    for i in range(n):
        weight = 0
        while weight < quorum_weight:
            (rcvTime, rcvWeight) = heapq.heappop(received[i])
            weight += rcvWeight
            completion_time[i] = rcvTime  
    
    arr = zip(completion_time, range(n))
    sorted_arr = sorted(arr, key=lambda x: x[0])
    return list(map(lambda x: x[1],sorted_arr))

def predict_latency(n, f, delta, weights, quorum_weight, faulty_replica, choice):
  faulty_replica_index = 4
  # Initialize validators
  validators = []
  if(not faulty_replica):
    validators = [Validator(i, weights[i], quorum_weight) for i in range(n)]
  else:
    validators = [Validator(i, weights[i], quorum_weight) for i in list(filter(lambda x: x!=faulty_replica_index, range(n)))]
    validators.insert(faulty_replica_index, Validator(faulty_replica_index, weights[faulty_replica_index], quorum_weight, faulty_replica))
  
  non_faulty_validators = validators.copy()
  del non_faulty_validators[faulty_replica_index]
  
  for validator in validators:
    validator.all_validators = validators
  # Simulate transactions and block creation
  for _ in range(rounds):
    for validator in validators:
      validator.receive_transaction(f"Validator {validator.id} - Transaction")
    for validator in validators:
        validator.advance_round()
  # Validate the state (example)
  # for validator in validators:
  #     print(f"Validator {validator.id} - weight {validator.faulty_replica} is at round {validator.certificate_creation_times[-1]}")



  if choice==1:
    #Optimize average latency
    if faulty_replica:
      res = (np.mean([validator.block_creation_times[-1] for validator in non_faulty_validators]), np.mean([validator.block_creation_times[-1] for validator in non_faulty_validators]))
    else:
      res = (np.mean([validator.block_creation_times[-1] for validator in validators]), np.mean([validator.block_creation_times[-1] for validator in validators]))
    # print(f"Average completion time of {rounds} rounds: {res}")
  elif choice==2:
    #Optimize maximum latency of a validator
    if faulty_replica:
      res = (np.max([validator.block_creation_times[-1] for validator in non_faulty_validators]), np.mean([validator.block_creation_times[-1] for validator in non_faulty_validators]))
    else:
      res = (np.max([validator.block_creation_times[-1] for validator in validators]), np.mean([validator.block_creation_times[-1] for validator in validators]))
  elif choice==3:
    #Optimize standard deviation of the latency a validator
    if faulty_replica:
      res = (np.std([validator.block_creation_times[-1] for validator in non_faulty_validators]), np.mean([validator.block_creation_times[-1] for validator in non_faulty_validators]))
    else:
      res = (np.std([validator.block_creation_times[-1] for validator in validators]), np.mean([validator.block_creation_times[-1] for validator in validators]))
  elif choice==4:
    #Optimize mean with standard deviation as tiebreaker
    if faulty_replica:
      res = (np.std([validator.block_creation_times[-1] for validator in non_faulty_validators]), np.mean([validator.block_creation_times[-1] for validator in non_faulty_validators]))
    else:
      res = (np.std([validator.block_creation_times[-1] for validator in validators]), np.mean([validator.block_creation_times[-1] for validator in validators]))
      
  return res

def exhaustive_search(n,f,delta, faulty_replica, choice):
    metric = 1000000
    std = 1000000
    consensusLatency = 0
    bestWeights = -1

    curConfig = 0
    for vMaxPos in combinations(range(n), 2*f):
        curWeights = [1] * n

        for i in vMaxPos:
            curWeights[i] = 1 + delta / f
        
        res = predict_latency(n, f, delta, curWeights, quorum_weight=2*(f+delta)+1, faulty_replica=faulty_replica, choice=choice)
        if choice == 4:
          if curConfig == 0 or res[1] < metric or (res[1] < 1.1*metric and res[0]<std):
              metric = res[1]
              std = res[0]
              consensusLatency = res[1]
              bestWeights = curWeights
        else:
          if curConfig == 0 or res[0] < metric:
            metric = res[0]
            consensusLatency = res[1]
            bestWeights = curWeights
        curConfig += 1

    # print('best consensus latency:', metric)
    # print('best weights:', bestWeights)
    return (bestWeights, consensusLatency)

def convert_bestweights_to_rmax_rmin(best_weights, vmax):
    replicas = [i for i in range(len(best_weights))]
    r_max = []
    r_min = []
    for rep_id in replicas:
        if best_weights[rep_id] == vmax:
            r_max.append(replicas[rep_id])
        else:
            r_min.append(replicas[rep_id])
    return r_max,r_min

def simulated_annealing(n, f, delta, faulty_replica, choice):
    # start = time.time()
    random.seed(500)

    step = 0
    step_max = 1000000
    temp = 120
    init_temp = temp
    theta = 0.0055
    t_min = 0.2
    r_max = []
    r_min = []

    curWeights = [1] * n
    for i in range(2*f):
            curWeights[i] = 1 + delta / f
    res = predict_latency(n, f, delta, curWeights, quorum_weight=2*(f+delta)+1, faulty_replica=faulty_replica, choice=choice)

    best_metric = res[0]
    best_lat = res[1]
    bestWeights = []
    jumps = 0

    while step < step_max and temp > t_min:
        replicaFrom = -1
        replicaTo = -1
        while True: 
            replicaFrom = random.randint(0, n-1) 
            if curWeights[replicaFrom] == 1+delta/f:
                break
        while True:
            replicaTo = random.randint(0, n-1)
            if replicaTo != replicaFrom:
                break
            
        newWeights = curWeights.copy()
        newWeights[replicaTo] = curWeights[replicaFrom]
        newWeights[replicaFrom] = curWeights[replicaTo]

        new_res = predict_latency(n, f, delta, newWeights, quorum_weight=2*(f+delta)+1, faulty_replica=faulty_replica, choice=choice)

        if choice == 4:
          if new_res[1] < res[1] or (new_res[1] < 1.1*res[1] and new_res[0]<res[0]):
            curWeights = newWeights
          else:
            rand = random.uniform(0,1)
            if rand < math.exp(-(new_res[1]-res[1]) / temp):
                jumps = jumps + 1
                curWeights = newWeights
          if new_res[1] < best_lat:
            best_metric = new_res[0]
            best_lat = new_res[1]
            bestWeights = newWeights

        else:
          if new_res[0] < res[0]:
              curWeights = newWeights
          else:
              rand = random.uniform(0,1)
              if rand < math.exp(-(new_res[0]-res[0]) / temp):
                  jumps = jumps + 1
                  curWeights = newWeights

          if new_res[0] < best_metric:
              best_metric = new_res[0]
              best_lat = new_res[1]
              bestWeights = newWeights


        temp = temp * (1-theta)
        step += 1

    return (bestWeights, best_lat)

def run_gain_experiment(faulty_replica, choice):
  best = exhaustive_search(n, f, delta=1, faulty_replica=faulty_replica, choice=choice)[1] # use exhaustive_search or simulated_annealing
  simple = predict_latency(n, f, delta=0, weights=[1 for _ in range(n)], quorum_weight= math.ceil((n+f+1)/2), faulty_replica=faulty_replica, choice=choice)
  # print(f'Gain : {simple-best}')
  return ((simple[1]-best)/simple[1]) * (-100)

def run_best_weights_experiment(faulty_replica, choice):
  best = exhaustive_search(n, f, delta=1, faulty_replica=faulty_replica, choice=choice)[1] # use exhaustive_search or simulated_annealing
  return best

# Simulation parameters
f = 3
n = 11

latency_matrix = df.iloc[:n, :n].values
print(m_utils.printMatrix(latency_matrix))


rounds = 2
tx_per_block = 1 

faulty_replica = False

n_experiments = 1

ok1=0

if ok1:
  choice=3
  best = exhaustive_search(n, f, delta=1, faulty_replica=False, choice=choice)
  metric = ""
  if choice==1:
    metric = "the average consensus latency"
  elif choice==2:
     metric = "the maximum consensus latency of node"
  elif choice==3:
     metric = "the standard deviation of the consensus latency"
  elif choice==4:
     metric = "the average and the standard deviation of the consensus latency"
  print(f'Optimizing for {metric}\nBest weights: {best[0]}\nConsensus Latency: {best[1]} ms')


gain_means = []
best_weights_means = []
best_weights_faulty_means = []

ok=1

if ok:
  for choice in [1,2,3,4]:
    gain_results = []
    best_weights_results = []
    best_weights_results_faulty = []

    gain_results.append(run_gain_experiment(faulty_replica, choice=choice))
    best_weights_results.append(run_best_weights_experiment(faulty_replica, choice=choice))
    best_weights_results_faulty.append(run_best_weights_experiment(not faulty_replica, choice=choice))
    
    gain_means.append(np.mean(gain_results))
    best_weights_means.append(np.mean(best_weights_results))
    best_weights_faulty_means.append(np.mean(best_weights_results_faulty))

# Create bar chart
fig, ax = plt.subplots(figsize=(14, 10))

bar_width = 0.5
index = np.arange(0, 4 * 2, 2)  # Adding more spacing between groups

bar1 = ax.bar(index, gain_means, bar_width, label='Latency Decrease (%)')
bar2 = ax.bar(index + bar_width, best_weights_means, bar_width, label='Optimal Weights Latency (ms)')
bar3 = ax.bar(index + 2 * bar_width, best_weights_faulty_means, bar_width, label='Optimal Weights with Faulty Replica Latency (ms)')

ax.set_xlabel('Objective Function', size=22)
ax.set_ylabel('Mean Values', size=22)
ax.set_xticks(index + bar_width)
ax.set_xticklabels(["Mean", "Max",
                    "Stddev", "Mean+Stddev"], size=22)
ax.legend(prop={'size': 20})

# Annotate bars with mean values
def annotate_bars(bars):
    for bar in bars:
        height = bar.get_height()
        ax.annotate(f'{height:.1f}',
                    xy=(bar.get_x() + bar.get_width() / 2, height),
                    xytext=(0, 3),
                    textcoords="offset points",
                    ha='center', va='bottom')

annotate_bars(bar1)
annotate_bars(bar2)
annotate_bars(bar3)

plt.tight_layout()
plt.show()